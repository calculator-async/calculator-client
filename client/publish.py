from calculatorclient.celery import app


def publish_add(x, y):
    payload = {"num_1": x, "num_2": y}
    publisher('calculator.add', payload)

def publish_div(x, y):
    payload = {"num_1": x, "num_2": y}
    publisher('calculator.div', payload)

def publish_mul(x, y):
    payload = {"num_1": x, "num_2": y}
    publisher('calculator.mul', payload)

def publish_sub(x, y):
    payload = {"num_1": x, "num_2": y}
    publisher('calculator.sub', payload)

def publisher(task_name, payload):
    app.send_task(task_name, [payload])