from django.shortcuts import render
from . import publish

# Create your views here.
def index(request):
    context = {}

    if request.method == "POST":
        if request.POST.get("publish"):
            num_1 = request.POST.get("num_1")
            num_2 = request.POST.get("num_2")
            type = request.POST.get("type_selector")

            if num_1 and num_2 and type:
                if type == "addition":
                    publish.publish_add(num_1, num_2)
                elif type == "division":
                    publish.publish_div(num_1, num_2)
                elif type == "multiplication":
                    publish.publish_mul(num_1, num_2)
                elif type == "substraction":
                    publish.publish_sub(num_1, num_2)
                else:
                    pass

    return render(request, 'index.html', context)
